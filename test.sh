#!/bin/bash

# -e 解释转义符
echo -e "Hi `user`, \n今天是: `date`"
# ls

# 打开ssh
echo -e "##\n##"
# 子shell运行不会影响当前shell的值
#

echo "开始执行文件[$0], 接收到参数数量:[$#], 参数1[$1], 参数1[$2], 参数1[$3]"
echo "脚本进程ID号:[$$], 后台运行的最后一个进程ID号:[$!], 参数:[$*], 参数:[$@]"

echo "-----------------"
echo "演示: \$* 等价 => \$@ => \"\$@\""
for i in $*; do
    echo "->[$i]"
done
echo ""
echo "演示: \"\$*\""
for i in "$*"; do
    echo "->[$i]"
done
echo ""
echo "演示: \"\$@\""
for i in "$@"; do
    echo "->[$i]"
done
echo "-----------------"


dir="test-docker"

echo "当前工作目录为: ${dir}"

runner_types=("shared" "group" "project")

echo "runner种类数量: ${#runner_types[@]}"

echo "runner种类分别是: ${runner_types[@]}"



echo "-------- expr ---------"
a=6
b=2
echo `expr ${a} + ${b} `
echo `expr ${a} - ${b} `
echo `expr ${a} \* ${b} `
echo `expr ${a} / ${b} `
# -o => or  -a => all
echo [ $a = $b ]
echo [ $a != $b ]
# -eq -ne -gt -lt -ge -le
echo [ $a -eq $b ]
a=$b
echo [ $a , $b ]
echo [ $a = $b ]
echo [ !false ]
# = !=
# -z => zero -n => not zero $=> empty
echo [ -z $b ]
printf "\033[32m SUCCESS: yay \033[0m\n";


printf "\033[32m SUCCESS: yay \033[0m\n";

echo "命令退出状态:[$?]"