#!/bin/bash

echo "go.sh"

docker_build_image

function docker_build_image()
{
	# 清理容器(保留历史构建的镜像)
	# destory_docker_service
 
	if [[ $? -eq 0 ]]; then
		echo "应用服务容器和镜像已处理，配置文件已完成复制!"
	else
		exit 1
	fi
   
	echo "Docker镜像构建......cmd:(docker build -t rtvsweb:$DOCKER_RTVSWEB_NEW_VERSION .)"
	docker build -t rtvsweb:$DOCKER_RTVSWEB_NEW_VERSION .
 
	# 判断是否有镜像,存在时创建相应的容器实例
	for i in [ `docker images` ]; do
		#statements
		if [[  "$i" == "$DOCKER_RTVSWEB_NEW_VERSION" ]]; then
			DOCKER_IMAGE_IS_MATCH_TAR_FILE="true"
			echo "已经找到最新构建的镜像!"
			run_docker_service_image
			break
		fi
	done
	if [[ $DOCKER_IMAGE_IS_MATCH_TAR_FILE == "false" ]]; then
		echo "构建镜像未匹配到最新版本，已退出安装!"
		exit 1
	fi
 
	echo "构建后的docker images镜像列表:"
	docker images
 
	echo "当前正在运行的Docker容器实例列表:"
	docker ps
}
